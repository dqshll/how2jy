#pragma once
#include "cocos2d.h"
#include "network/WebSocket.h"
#include <string>
#include <vector>

using namespace std;
using namespace cocos2d;


class Utility : public Ref{
public:
    static Utility* getInstance();
    Utility ();
    
    void setVoteResult(int groupId, string path0, string path1);
    vector<std::string> getVoteResult (int groupId);
    static float getSuitableScale(Sprite* sprite, Size);
    std::string getLocalUsablePath();
    void readSettingFile();
    int getVoteTimeInSec();
private:
    //void readSettingFile();
 
private:
    static Utility* sharedUtility;
    vector <string> mGroupResult0;
    vector <string> mGroupResult1;
    vector <string> mGroupResult2;
    
    int mVoteTimeInSec;
};
