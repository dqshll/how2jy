//
//  VoteItem.hpp
//  FlipDog
//
//  Created by dingqun on 2017/10/31.
//

#pragma once
#include "cocos2d.h"
#define MAX_RECT_NUM        (14)

using namespace cocos2d;

class VoteItem : public Sprite {
public:
    //VoteItem();
    bool initWithIndex(int);

    void setScore(int);

private:
    int mScore;
    Label* mPiao;
};
