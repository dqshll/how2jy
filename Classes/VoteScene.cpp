#include "VoteScene.h"
#include "SimpleAudioEngine.h"
#include "GroupScene.h"
#include "Utility.h"
#include "NetworkControler.h"

#define TAG_TITLE_LABEL         (124)
#define TAG_ENVOLOP             (221)
#define TAG_INFO_LABEL          (81)
#define PIC_SPRITE_TAG          (23)
#define VOTE_ITEM_TAG           (100)

#define PIC_DISPLAY_SCALE       (3.0)
#define PIC_DISPLAY_INTERVAL    (3.0)
#define PIC_SCALE_INTERVAL      (0.6f)




static Vec2 sPicPosition[5];
static Vec2 sTargetPosition;

Scene* VoteScene::createScene(){
    return VoteScene::create();
}

// on "init" you need to initialize your instance
bool VoteScene::initWithStarId(int index, const std::string& title) {
    if ( !Scene::init() ) {
        return false;
    }
    
    mStarIndex = index;
    mKeyBuff.clear();

    NetworkControler::getInstance()->beginVote(mStarIndex);
    
    mTimeCounter = Utility::getInstance()->getVoteTimeInSec();

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    float space = visibleSize.width * 0.156;
    
    sPicPosition[0] = Vec2(visibleSize.width * 0.12, visibleSize.height * 0.5);
    sPicPosition[1] = Vec2(sPicPosition[0].x + space, sPicPosition[0].y);
    sPicPosition[2] = Vec2(sPicPosition[1].x + space, sPicPosition[0].y);
    sPicPosition[3] = Vec2(sPicPosition[2].x + space, sPicPosition[0].y);
    sPicPosition[4] = Vec2(sPicPosition[3].x + space, sPicPosition[0].y);

    sTargetPosition = Vec2(sPicPosition[3].x, sPicPosition[3].y + space * 0.5);

    auto sprite = Sprite::create("bg_2.png");
    sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
    this->addChild(sprite, 0);

    auto thumb = Sprite::create(getStarThumbFile());
    thumb->setPosition(Vec2(10, visibleSize.height - 10));
    thumb->setAnchorPoint(Vec2(0, 1));
    thumb->setScale(0.6);
    this->addChild(thumb, 1);
    
    auto label = Label::createWithTTF(title, "fonts/ms_yahei.ttf", 30);
    label->setPosition(Vec2(thumb->getPosition().x + thumb->getBoundingBox().size.width + 40, thumb->getPosition().y - thumb->getBoundingBox().size.height * 0.5));
    label->setTag(TAG_TITLE_LABEL);
    this->addChild(label, 1);
    
    this->createPicBezerMove();
    this->createVoteBars();
    
    Label* infoLabel = Label::createWithTTF("", "fonts/ms_yahei.ttf", 24);
    infoLabel->setPosition(visibleSize.width * 0.5, visibleSize.height * 0.5);
    infoLabel->setVisible(false);
    infoLabel->setTag(TAG_INFO_LABEL);
    this->addChild(infoLabel, 3);
    
    this->createTimeCounter();
    
    this->registerKeyPadListener();
    return true;
}

std::string VoteScene::getStarThumbFile() {
    std::string filename;
    switch (mStarIndex) {
    case IDX_STAR_0:
        filename = "star_thumb_0.png";
        break;
    case IDX_STAR_1:
        filename = "star_thumb_1.png";
        break;
    case IDX_STAR_2:
        filename = "star_thumb_2.png";
        break;
    default:
        break;
    }
    return filename;
}

void VoteScene::createPicBezerMove() {
    Sprite* envolop = Sprite::create("xinfeng.png");
    Size visibleSize = Director::getInstance()->getVisibleSize();
    envolop->setPosition(Vec2(visibleSize.width * 0.35, visibleSize.height * 0.85));
    envolop->setTag(TAG_ENVOLOP);
    this->addChild(envolop, 2);
    
    float space_0 = 20;
    Sprite* stampRight = Sprite::create("stamp.png");
    stampRight->setPosition(Vec2(visibleSize.width - space_0, visibleSize.height - space_0));
    stampRight->setAnchorPoint(Vec2(1, 1));
    this->addChild(stampRight, 0);
    
    float xSpace = 42;
    float ySpace = 50;
    float barCodeScale = 0.7;
#if FORMAL_SERVER
    Sprite* voteBarcodeRight = Sprite::create("vote_barcode.png");
#else
    Sprite* voteBarcodeRight = Sprite::create("vote_barcode_d.png");
#endif
    voteBarcodeRight->setPosition(Vec2(visibleSize.width - xSpace, visibleSize.height - ySpace));
    voteBarcodeRight->setAnchorPoint(Vec2(1,1));
    voteBarcodeRight->setScale(barCodeScale);
    this->addChild(voteBarcodeRight, 1);
    
    Sprite* logoRight = Sprite::create("logo_right.png");
    logoRight->setPosition(Vec2(visibleSize.width - 10, 10));
    logoRight->setAnchorPoint(Vec2(1, 0));
    this->addChild(logoRight, 1);
    
    Sprite* logo3 = Sprite::create("logo3.png");
    logo3->setPosition(Vec2(visibleSize.width - 10, 20));
    logo3->setAnchorPoint(Vec2(1, 0));
    this->addChild(logo3, 1);
    
    std::string path = Utility::getInstance()->getLocalUsablePath();
    
    char buff[256] = { 0 };
    sprintf(buff, "%s/%d/", path.c_str(), mStarIndex);
//    CCLOG(buff);
    //FILE * pLogFile = fopen("cclog.txt", "a+");
    //if (pLogFile) {
    //    fputs(buff, pLogFile);
    //    fflush(pLogFile);
    //    fclose(pLogFile);
    //}

    std::vector<std::string> rawPaths = FileUtils::getInstance()->listFiles(buff);
    
    std::vector<std::string> paths;

    for (int i = 0; i < rawPaths.size(); i++) {
        int pos = rawPaths[i].find("/.");
        if (pos != -1) {
            continue;
        }
//        pos = rawPaths[i].find("/../");
//        if (pos != -1) {
//            continue;
//        }
        
        paths.push_back(rawPaths[i]);
    }

//    float dur0 = 1;
//    float scaleDur = 0.3;
//    float interval0 = 1;
//    float flyDur = dur0 + interval0 * TOP_NUM + 1;

    int n = TOP_NUM;
    if (n > paths.size()) {
        n = paths.size();
    }
    mNum = n;
    for (int i = 0; i < n; i++) {
        mPicPaths[i] = paths.at(i);
        Sprite* pic = Sprite::create(paths.at(i));
        pic->setPosition(sPicPosition[i]);
        this->addChild(pic);
        pic->setTag(PIC_SPRITE_TAG + i);
        float originScale = Utility::getSuitableScale(pic, Size(visibleSize.width * 0.151, visibleSize.height * 0.277));
        
        sprintf(buff, "hao%d.png", i);
        Sprite* label = Sprite::create(buff);
        label->setPosition(Vec2(pic->getBoundingBox().size.width * 0.5, pic->getBoundingBox().size.height * 1.1));
        pic->setScale(originScale);
        label->setScale(1.0/originScale);
        pic->addChild(label, 2);

//        pic->setScale(.01);

//        Vec2 pos1 = Vec2(visibleSize.width*0.25, visibleSize.height * 0.8);
//        Vec2 pos2 = Vec2(visibleSize.width*0.1, visibleSize.height * 0.7);

//        ccBezierConfig cfg;
//        cfg.controlPoint_1 = pos1;
//        cfg.controlPoint_2 = pos2;
//        cfg.endPosition = sPicPosition[i];
//        ActionInterval *bezier = BezierTo::create(1.0, cfg);

//        CallFuncN* callback = NULL;
//        if (i == n - 1) { // last one
//            callback = CallFuncN::create(CC_CALLBACK_0(VoteScene::goThroughPics, this, n));
//        }
//        Spawn* spawn = Spawn::create(ScaleTo::create(scaleDur, originScale), bezier, NULL);
//        Sequence* seq = Sequence::create(DelayTime::create(dur0 + interval0 * i), spawn,
//            DelayTime::create(interval0), callback, NULL);
//        pic->runAction(seq);
    }
}

void VoteScene::goThroughPics(int n) {

    for (int i = 0; i < n; i++) {
        Sprite* pic = (Sprite*) this->getChildByTag(PIC_SPRITE_TAG + i);

        CallFuncN* callback = CallFuncN::create(CC_CALLBACK_0(VoteScene::repeatOnePicScaleInOut, this, PIC_SPRITE_TAG + i, n));
        Sequence* seq = Sequence::create(DelayTime::create((PIC_DISPLAY_INTERVAL + PIC_SCALE_INTERVAL * 2) * i), callback, NULL);
        pic->runAction(seq);
    }
}

void VoteScene::repeatOnePicScaleInOut(int tag, int n) {
    float scale = PIC_DISPLAY_SCALE;

    Sprite* pic = (Sprite*) this->getChildByTag(tag);
    float originScale = pic->getScale();
    Spawn* spawn = Spawn::create(ScaleTo::create(PIC_SCALE_INTERVAL, originScale * scale), MoveTo::create(PIC_SCALE_INTERVAL, sTargetPosition), NULL);
    Spawn* reverseSpawn = Spawn::create(ScaleTo::create(PIC_SCALE_INTERVAL, originScale), MoveTo::create(PIC_SCALE_INTERVAL, sPicPosition[tag - PIC_SPRITE_TAG]), NULL);

    CallFuncN* preCallback = CallFuncN::create(CC_CALLBACK_0(VoteScene::setPicsZOrder, this, tag, true, n));
    CallFuncN* postCallback = CallFuncN::create(CC_CALLBACK_0(VoteScene::setPicsZOrder, this, tag, true, n));

    Sequence* scaleInOut = Sequence::create(preCallback, spawn, DelayTime::create(PIC_DISPLAY_INTERVAL), reverseSpawn, 
        DelayTime::create((PIC_DISPLAY_INTERVAL + PIC_SCALE_INTERVAL * 2) * (n - 1)), postCallback, NULL);
    RepeatForever* repeat = RepeatForever::create(scaleInOut);
    pic->runAction(repeat);
}

void VoteScene::setPicsZOrder(int tag, bool upOrDown, int n) {
    for (int i = 0; i < n; i++) {
        Sprite* pic = (Sprite*) this->getChildByTag(PIC_SPRITE_TAG + i);
        if (i == tag - PIC_SPRITE_TAG){
            pic->setZOrder(upOrDown ? 3 : 2);
        } else {
            pic->setZOrder(upOrDown ? 2 : 3);
        }
    }
}

void VoteScene::createVoteBars() {
    Size visibleSize = Director::getInstance()->getVisibleSize();
    float space = visibleSize.width *0.06;
   
    //int test_score[TOP_NUM] = {180, 100, 99, 41, 40};

    for (int i = 0; i < mNum; i++) {
        float x = visibleSize.width * 0.55 + i * space;
        VoteItem* item = (VoteItem*) VoteItem::create();
        item->initWithIndex(i);
        item->setPosition(Vec2(sPicPosition[i].x, sPicPosition[i].y  - visibleSize.height * 0.3));
        item->setTag(VOTE_ITEM_TAG + i);
        //item->setScore(test_score[i]);
        this->addChild(item, 1);
        mVoteItems[i] = item;
    }
}

void VoteScene::createTimeCounter() {
    char buff[32] = { 0 };
    sprintf(buff, "%02d", mTimeCounter);
    Size visibleSize = Director::getInstance()->getVisibleSize();
    this->mTimerLabel = LabelBMFont::create(buff, "clock.fnt", 180, TextHAlignment::CENTER);
    this->mTimerLabel->setPosition(Vec2(visibleSize.width * 0.56, visibleSize.height * 0.85));
    this->addChild(this->mTimerLabel, 2);


    this->mTimerScheduler = schedule_selector(VoteScene::updateTimeCounter);
    this->schedule(this->mTimerScheduler, 1.0f);
    this->scheduleUpdate();
}

void VoteScene::onEnter() {
    Scene::onEnter();
    CCLOG("VoteScene::onEnter");
    NetworkControler::getInstance()->setVoteDelegator(this);
}

void  VoteScene::onExit() {
    CCLOG("VoteScene::onExit");
    NetworkControler::getInstance()->setVoteDelegator(NULL);
    Director::getInstance()->getEventDispatcher()->removeEventListener(mKeyboardListener);
    Scene::onExit();
}

void VoteScene::updateTimeCounter(float dt) {
    if (mTimeCounter <= 0) {
        this->unschedule(this->mTimerScheduler);
        NetworkControler::getInstance()->endVote(mStarIndex); // stop voting
        mTimerLabel->setVisible(false);
        Sprite* finishSprite = Sprite::create("finish.png");
        finishSprite->setPosition(mTimerLabel->getPosition());
        this->addChild(finishSprite);
        this->VoteFinish();
    } else {
        mTimeCounter--;
        char buff[32] = { 0 };
        sprintf(buff, "%02d", mTimeCounter);
        mTimerLabel->setString(buff);
    }
}

int VoteScene::getScoreAt (int i) {
    return mNetScores[i] + mLocalScores[i];
}

void VoteScene::VoteFinish () {
    
    int top0 = 0;
    for (int i=1; i<TOP_NUM; i++) {
        if (this->getScoreAt(top0) < this->getScoreAt(i)) {
            top0 = i;
        }
    }
    
    int copyScore[TOP_NUM] = {0};
    for (int i=0; i<TOP_NUM; i++) {
        copyScore[i] = this->getScoreAt(i);
    }
    copyScore[top0] = -1;
    
    int top1 = 0;
    for (int i=1; i<TOP_NUM; i++) {
        if (copyScore[top1] < copyScore[i]) {
            top1 = i;
        }
    }
    
    Utility::getInstance()->setVoteResult(mStarIndex, mPicPaths[top0], mPicPaths[top1]);
}



void VoteScene::onConnected(void) {
    Label* infoLabel = (Label*) this->getChildByTag(TAG_INFO_LABEL);
    infoLabel->setVisible(false);
    infoLabel->setString("");
}

void VoteScene::onDisconnected(void) {
    Label* infoLabel = (Label*) this->getChildByTag(TAG_INFO_LABEL);
    infoLabel->setVisible(true);
    infoLabel->setString("网络连接已断开");
}

void VoteScene::onError(int errCode) {
    Label* infoLabel = (Label*) this->getChildByTag(TAG_INFO_LABEL);
    infoLabel->setVisible(true);
    char buff[32] = { 0 };
    sprintf(buff, "网络连接错误码 = %d", errCode);
    infoLabel->setString(buff);
}

void VoteScene::onVoteScore(int starId, int candIndex, int score) {
    if (starId == mStarIndex && candIndex < TOP_NUM) {
        int sc = mNetScores[candIndex];
        if (sc < score){
            mNetScores [candIndex] = score;
            mVoteItems[candIndex]->setScore(this->getScoreAt(candIndex));
        }
    }
}

void VoteScene::registerKeyPadListener () {
    mKeyboardListener = EventListenerKeyboard::create();
    mKeyboardListener->onKeyReleased =CC_CALLBACK_2(VoteScene::onKeyboardReleased, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(mKeyboardListener, 2);
}

void VoteScene::onKeyboardReleased(EventKeyboard::KeyCode keyCode, Event* e) {
    CCLOG("keycode pressed = %d", keyCode);
    if(mKeyBuff.size() >= 2) {
        mKeyBuff.clear();
    }
    
    mKeyBuff.push_back(keyCode);
    if (mKeyBuff.size() == 1) {
        if(mTimeCounter > 0 && mKeyBuff [0] >= (EventKeyboard::KeyCode) 77 && mKeyBuff [0] <= (EventKeyboard::KeyCode) 81) {
            int candIndex = (int)mKeyBuff[0] - 77;
            mLocalScores[candIndex] ++;
            mVoteItems[candIndex]->setScore(this->getScoreAt(candIndex));
        }
        
        if (mKeyBuff [0] != (EventKeyboard::KeyCode) 128) {
            mKeyBuff.clear();
        }
    } else if (mKeyBuff.size() == 2) {
        if (mKeyBuff [0] == (EventKeyboard::KeyCode) 128 && // E
            mKeyBuff [1] == (EventKeyboard::KeyCode) 145) { // V
            if(mTimeCounter <= 0) {
                CCLOG("EV -> gourp %d is over", mStarIndex);
                Director::getInstance()->popScene();
            }
        } else {
            mKeyBuff.clear();
        }
    }
}
