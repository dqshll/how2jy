#include "NetworkControler.h"
#include <cstdlib>
#include <stdlib.h>
#include "VoteScene.h"

#define RELEASE_HOST  "ws://h5.edisonx.cn"
#define DEBUG_HOST  "ws://114.115.205.206"

#if FORMAL_SERVER
#define SERVER_HOST     RELEASE_HOST
#else
#define SERVER_HOST     DEBUG_HOST
#endif

#define CONNECT_RETRY_DELAY_TIME    (5)

NetworkControler* NetworkControler::sharedNetworkControler = nullptr;

NetworkControler* NetworkControler::getInstance(){
    if(sharedNetworkControler == NULL) {
        sharedNetworkControler = new NetworkControler();
    }
    return sharedNetworkControler;
}

NetworkControler::NetworkControler () {
    m_pServerWebSocket = NULL;
    m_pBuzzWebSocket = NULL;
    m_pVoteDelegator = NULL;
    m_pStatusDelegator = NULL;
//    this->connect2Server();
    this->connect2Buzz(2350);
}

void NetworkControler::connect2Server() {
    
    m_pServerWebSocket = new WebSocket();
    
    ServerDelegator* serverDelegator = new ServerDelegator();
    string host = SERVER_HOST;
    host.append(":3000");
    CCLOG("connecting to server %s ...", host.c_str());
    m_pServerWebSocket->init(*serverDelegator, host);//ali主服务器
}

void NetworkControler::connect2Buzz(int port) {
    CCLOG("connecting to buzz (port=%d)...", port);
    
    if (m_pServerWebSocket) {
        m_pServerWebSocket->close();
        m_pServerWebSocket = NULL;
    }
    
    m_pBuzzWebSocket = new WebSocket();

    char buff [32] = {0};
    sprintf (buff, "%s:%d", SERVER_HOST, port);

    CCLOG("connecting to buzz %s ...", buff);
    m_pBuzzWebSocket->init(*this, buff);//ali主服务器
    //m_pBuzzWebSocket->init(*this, "ws://101.201.38.200:2347");//ali主服务器
}

ServerDelegator::ServerDelegator() {}

void ServerDelegator::onOpen(WebSocket * ws) {
    CCLOG("Server OnOpen");
}

void ServerDelegator::onMessage(WebSocket * ws, const WebSocket::Data & data) {
    string textStr = data.bytes;
    CCLOG("Server onMessage raw=%s", textStr.c_str());
    int n = textStr.find_last_of(":");
    if (n != -1) {
        textStr.erase(0, n + 1);
        // use fix port for this project
        int port = 2350; //atoi(textStr.c_str());
        NetworkControler::getInstance()->connect2Buzz(port);
    }
}

void ServerDelegator::onClose(WebSocket * ws) {
    CCLOG("Server onClose");
}

void ServerDelegator::onError(WebSocket * ws, const WebSocket::ErrorCode & error) {
    char buf[128] = { 0 };
    sprintf(buf, "Error was fired, error code: %d", error);
    CCLOG("Server onError = %s", buf);
}

void NetworkControler::setVoteDelegator(NetworkVoteDelegate* value) {
    this->m_pVoteDelegator = value;
}

void NetworkControler::setStatusDelegator(NetworkStatusDelegate* value) {
    this->m_pStatusDelegator = value;
}

void NetworkControler::onOpen(WebSocket * ws) {
    CCLOG("OnOpen");
    m_pBuzzWebSocket->send(std::string("box:jy"));
    if (m_pStatusDelegator) {
        m_pStatusDelegator->onConnected();
    }

    if (m_pVoteDelegator) {
        m_pVoteDelegator->onConnected();
    }
}

void NetworkControler::decodeCommand (const string& command, string& nickname, string& thumbUrl, int& userId) {
    vector<string> splits;
    string l = ";";
    NetworkControler::split(command, l, &splits);
    if(splits.size() >= 5) {
        nickname.assign(splits.at(1));
        thumbUrl.assign(splits.at(2));
        const char* tmp = splits.at(4).c_str();
        userId = atoi(tmp);
    }
}

void NetworkControler::onMessage(WebSocket * ws, const WebSocket::Data & data) {
    string textStr = data.bytes;
    CCLOG("onMessage raw=%s", textStr.c_str());

    if (textStr.find_first_of("s:") == 0) { // one score
        textStr.erase(0, 2);
        this->decodeGroupScores(textStr);
    }
}

void NetworkControler::decodeGroupScores (const string& rawData) {
    vector<string> splits;
    string l = "@";
    split(rawData, l, &splits);
    for (int i=0; i<splits.size(); i++) {
        vector<string> tmps;
        string tmp = splits[i];
        l = "|";
        split(tmp, l, &tmps);
        
        int starId = atoi(tmps[0].c_str());
        tmp = tmps[1];
        
        l = ",";
        split(tmp, l, &tmps);
        
        for (int j=0; j<tmps.size(); j++) {
            vector<string> pair;
            string ll = "=";
            split(tmps[j], ll, &pair);
            
            int candIndex = atoi(pair[0].c_str());
            int score = atoi(pair[1].c_str());
            if (m_pVoteDelegator) {
                m_pVoteDelegator->onVoteScore(starId, candIndex, score);
            }
        }
        
    }
}

void NetworkControler::onClose(WebSocket * ws) {
    CCLOG("onClose");
    if (ws == m_pBuzzWebSocket)
    {
        CC_SAFE_DELETE(m_pBuzzWebSocket);
    }
    m_pBuzzWebSocket = NULL;
    
    if (m_pVoteDelegator) {
        m_pVoteDelegator->onDisconnected();
    }
    if (m_pStatusDelegator) {
        m_pStatusDelegator->onDisconnected();
    }
    
    CCLOG("try reconnect");

    CallFunc* callback = CallFunc::create([&]() {
        CCLOG("run action callback");
        this->connect2Server();
    });
//    GameLayer::getInstance()->runAction(Sequence::create(DelayTime::create(CONNECT_RETRY_DELAY_TIME), callback, NULL));
}

void NetworkControler::onError(WebSocket * ws, const WebSocket::ErrorCode & error) {
    CCLOG("onError");
    char buf[64] = { 0 };
    if (ws == m_pBuzzWebSocket)
    {
        sprintf(buf, "an error was fired, code: %d", error);
    } else {
        sprintf(buf, "Error was fired, error code: %d", error);
    }
    if (m_pVoteDelegator) {
        m_pVoteDelegator->onError((int)error);
    }  
    if (m_pStatusDelegator) {
        m_pStatusDelegator->onError((int)error);
    }
}

void NetworkControler::split(const std::string& s, std::string& delim,std::vector< std::string >* ret) {
    if(ret->size() > 0) {
        ret->clear();
    }
    size_t last = 0;
    size_t index=s.find_first_of(delim,last);
    while (index!=std::string::npos) {
        ret->push_back(s.substr(last,index-last));
        last=index+1;
        index=s.find_first_of(delim,last);
    }
    
    if (index-last>0) {
        ret->push_back(s.substr(last,index-last));
    }
}

//void NetworkControler::kickUser(int socketId) {
//    CCLOG("kickUser");
//    if (m_pBuzzWebSocket && socketId >0) {
//        char buf[64] = { 0 };
//        sprintf(buf, "%d", socketId);
//        string strKick (buf);
//        strKick.append("-kick");
//        m_pBuzzWebSocket->send(strKick);
//    }
//}
//
//void NetworkControler::replyUserIamPlaying(int socketId) {
//    CCLOG("replyUserIamPlaying");
//    if (m_pBuzzWebSocket && socketId >0) {
//        char buf[32] = { 0 };
//        sprintf(buf, "%d-p", socketId);
//        m_pBuzzWebSocket->send(buf);
//    }
//}
//
//void NetworkControler::replyUserIamWaiting(int socketId, int n) {
//    CCLOG("replyUserIamWaiting");
//    if (m_pBuzzWebSocket && socketId >0) {
//        char buf[32] = { 0 };
//        sprintf(buf, "%d-w%d", socketId, n);
//        m_pBuzzWebSocket->send(buf);
//    }
//}
//
//void NetworkControler::updateUserScore(int socketId, int score) {
//    CCLOG("updateUserScore");
//    if (m_pBuzzWebSocket && socketId >0) {
//        char buf[32] = { 0 };
//        sprintf(buf, "%d-s%d", socketId, score);
//        m_pBuzzWebSocket->send(buf);
//    }
//}

void NetworkControler::finishVote() {
    CCLOG("finishVoteø");
    if (m_pBuzzWebSocket) {
        m_pBuzzWebSocket->send("fs:");
    }
}

void NetworkControler::heartBeat() {
    CCLOG("heartBeat");
    if (m_pBuzzWebSocket) {
        m_pBuzzWebSocket->send("heart");
    }
}


void NetworkControler::beginVote(int groupId) {
    CCLOG("beginVote %d", groupId);
    if (m_pBuzzWebSocket) {
        char buf[32] = { 0 };
        sprintf(buf, "bv:%d", groupId);
        m_pBuzzWebSocket->send(buf);
    }
}

void NetworkControler::endVote(int groupId) {
    CCLOG("endVote");
    if (m_pBuzzWebSocket) {
        char buf[32] = { 0 };
        sprintf(buf, "ev:%d", groupId);
        m_pBuzzWebSocket->send(buf);
    }
}

//void NetworkControler::fetchVoteScore() {
//    CCLOG("fetchVoteScore");
//    if (m_pBuzzWebSocket) {
//        m_pBuzzWebSocket->send("ft");
//    }
//}

