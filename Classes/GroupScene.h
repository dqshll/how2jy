#ifndef __GroupScene_SCENE_H__
#define __GroupScene_SCENE_H__

#include "cocos2d.h"
#include "NetworkControler.h"
#include "CCEventKeyboard.h"

class GroupScene : public cocos2d::Scene, NetworkStatusDelegate
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuStarCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(GroupScene);

    virtual void onConnected(void);
    virtual void onDisconnected(void);
    virtual void onError(int errCode);
    virtual void onExit();
    virtual void onEnter();
    
private:
    void registerKeyPadListener ();
    void onKeyboardReleased(EventKeyboard::KeyCode keyCode, Event* e);
    void toVoteScene (int starIndex);
    void displayResult ();
    void ScaleOutIn(int index);
    void createResultPic(int, Sprite*);
    void createVersionDisplay();
private:
    EventListenerKeyboard* mKeyboardListener;
    std::vector<EventKeyboard::KeyCode> mKeyBuff;
    Menu* mMenu;
    Vec2 mPicPosotions[6];
};

#endif // __GroupScene_SCENE_H__
