//
//  VoteItem.cpp
//  FlipDog
//
//  Created by dingqun on 2017/10/31.
//

#include "VoteItem.h"
#include <stdio.h>

#define TAG_INDEX           (21)
#define TAG_SCORE           (33)
#define TAG_BAR_0           (208)

using namespace std;

//VoteItem::VoteItem() {
//}

bool VoteItem::initWithIndex(int index) {
    Size visibleSize = Director::getInstance()->getVisibleSize();

    char buff[16] = { 0 };
    sprintf(buff, "hao%d.png", index);
//    Sprite* indexLabel = Sprite::create(buff);
//    indexLabel->setPosition(Point(0, 0));
//    this->addChild(indexLabel);

//    float x = visibleSize.height * 0.0;
//    float y = visibleSize.height * 0.05;
//    float space = visibleSize.height * 0.04;
    
    mPiao = Label::createWithTTF("0", "fonts/led.ttf", 120);

//    for (int i=0; i<MAX_RECT_NUM; i++) {
//        sprintf(buff, "bar%d.png", i);
//
//        Sprite* bar = Sprite::create(buff);
//        bar->setPosition(Vec2(x, y + space * i));ø
//        if (i == 0) {
//            mPiao->setPosition(bar->getPosition());
//        }
//        bar->setVisible(false);
//        bar->setTag(TAG_BAR_0 +  i);
//        this->addChild(bar);
//    }
 
    this->addChild(mPiao);
  
    return true;
}

void VoteItem::setScore (int score) {
    char buff[32] = { 0 };
    sprintf(buff, "%d", score);
    mPiao->setString(buff);

//    int n = 0;
//    if (score <= 40) {
//        n = int (floor(score / 5));
//    } else if (n>=100) {
//        n = 14;
//    } else {
//        n = int(floor(score - 40) / 10) + 8;
//    }
//
//    for (int i = 0; i < MAX_RECT_NUM; i++) {
//        Sprite* bar = (Sprite*) this->getChildByTag(TAG_BAR_0 + i);
//        bar->setVisible(i < n);
//    }
//
//    Size visibleSize = Director::getInstance()->getVisibleSize();
//    float piaoOffsetY = visibleSize.height *0.04;
//
//    if (n < MAX_RECT_NUM) {
//        Sprite* bar = (Sprite*) this->getChildByTag(TAG_BAR_0 + n);
//        mPiao->setPosition(bar->getPosition());
//    } else {
//        Sprite* bar = (Sprite*) this->getChildByTag(TAG_BAR_0 + MAX_RECT_NUM - 1);
//        Vec2 pos = bar->getPosition();
//        mPiao->setPosition(pos.x, pos.y + piaoOffsetY);
//    }
   
}
