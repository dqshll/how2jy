#pragma once
#include "cocos2d.h"
#include "network/WebSocket.h"

#define FORMAL_SERVER       (1)

using namespace std;
using namespace cocos2d;
using namespace cocos2d::network;

class NetworkVoteDelegate {
public:
    virtual void onConnected(void) = 0;
    virtual void onDisconnected(void) = 0;
    virtual void onError(int errCode) = 0;
    virtual void onVoteScore(int starId, int candIndex,  int score) = 0;
};

class NetworkStatusDelegate {
public:
    virtual void onConnected(void) = 0;
    virtual void onDisconnected(void) = 0;
    virtual void onError(int errCode) = 0;
};

class ServerDelegator: public Ref , public WebSocket::Delegate{
public:
    ServerDelegator ();
    virtual void onOpen(WebSocket* ws);
    virtual void onMessage(WebSocket* ws, const WebSocket::Data& data);
    virtual void onClose(WebSocket* ws);
    virtual void onError(WebSocket* ws, const WebSocket::ErrorCode& error);
};

class NetworkControler : public Ref , public WebSocket::Delegate{
public:
    static NetworkControler* getInstance();
    NetworkControler ();
    void setVoteDelegator(NetworkVoteDelegate*);
    void setStatusDelegator(NetworkStatusDelegate*);
    void connect2Buzz(int port);

    void finishVote();
    void heartBeat();
    void beginVote(int);
    void endVote(int);
//    void fetchVoteScore();
private:
    static NetworkControler* sharedNetworkControler;
    
    virtual void onOpen(WebSocket* ws);
    virtual void onMessage(WebSocket* ws, const WebSocket::Data& data);
    virtual void onClose(WebSocket* ws);
    virtual void onError(WebSocket* ws, const WebSocket::ErrorCode& error);
    
    void decodeCommand (const string& command, string& nickname, string& thumbUrl, int&);
    static void split(const std::string& s, std::string& delim,std::vector< std::string >* ret);
    void connect2Server();
    void decodeGroupScores (const string& rawData);

private:
    WebSocket* m_pServerWebSocket;
    WebSocket* m_pBuzzWebSocket;
    NetworkVoteDelegate* m_pVoteDelegator;
    NetworkStatusDelegate* m_pStatusDelegator;
};
