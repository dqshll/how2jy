#ifndef __VoteScene_SCENE_H__
#define __VoteScene_SCENE_H__

#include "cocos2d.h"
#include <string>
#include "VoteItem.h"
#include "NetworkControler.h"
#include "CCEventKeyboard.h"
USING_NS_CC;

#define TOP_NUM     5

#define IDX_STAR_0  (0)
#define IDX_STAR_1  (1)
#define IDX_STAR_2  (2)

class VoteScene : public cocos2d::Scene, NetworkVoteDelegate
{
public:
    static cocos2d::Scene* createScene();

    virtual bool initWithStarId(int,  const std::string&);
    
    virtual void onEnter();
    virtual void onExit();
    
    CREATE_FUNC(VoteScene);
    virtual void onConnected(void);
    virtual void onDisconnected(void);
    virtual void onError(int errCode);
    virtual void onVoteScore(int starId, int candIndex, int score);

private:
    void createPicBezerMove();
    std::string getStarThumbFile();
    void goThroughPics(int n);
    void repeatOnePicScaleInOut(int tag, int n);
    void setPicsZOrder(int tag, bool upOrDown, int n);
    void createVoteBars();
    void createTimeCounter();
    void updateTimeCounter(float);

    void registerKeyPadListener ();
    void onKeyboardReleased(EventKeyboard::KeyCode keyCode, Event* e);
    void VoteFinish ();
    int getScoreAt (int i);
private:
    LabelBMFont* mTimerLabel;
    SEL_SCHEDULE  mTimerScheduler;
    int mStarIndex;
    int mNum;
    int mTimeCounter;
    std::string mPicPaths[TOP_NUM];
    int mNetScores[TOP_NUM] = {0};
    int mLocalScores [TOP_NUM] = {0};
    VoteItem* mVoteItems[TOP_NUM] = {0};
    EventListenerKeyboard* mKeyboardListener;
    std::vector<EventKeyboard::KeyCode> mKeyBuff;
};

#endif // __VoteScene_SCENE_H__
