#include "WaitScene.h"
#include "SimpleAudioEngine.h"
#include "GroupScene.h"


#define TAG_INFO_LABEL   (86)

USING_NS_CC;

Scene* WaitScene::createScene()
{
    return WaitScene::create();
}

// on "init" you need to initialize your instance
bool WaitScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto enterItem = MenuItemImage::create(
                                           "btn_enter_0.png",
                                           "btn_enter_0.png",
                                           CC_CALLBACK_1(WaitScene::menuEnterCallback, this));

    float x = visibleSize.width * 0.5 ;
    float y = visibleSize.height * 0.2;
    enterItem->setPosition(Vec2(x, y));

    // create menu, it's an autorelease object
    auto menu = Menu::create(enterItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
    Sequence* seq = Sequence::create(ScaleTo::create(0.5, 0.6), DelayTime::create(0.5), ScaleTo::create(0.5, 1) , DelayTime::create(0.2), NULL);
    RepeatForever* repeat = RepeatForever::create(seq);
    enterItem->runAction(repeat);

    // add "WaitScene" splash screen"
    auto sprite = Sprite::create("bg_0.png");
    sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
    this->addChild(sprite, 0);

    Label* infoLabel = Label::createWithTTF("", "fonts/ms_yahei.ttf", 24);
    infoLabel->setPosition(visibleSize.width * 0.5, visibleSize.height * 0.5);
    infoLabel->setVisible(false);
    infoLabel->setTag(TAG_INFO_LABEL);
    this->addChild(infoLabel, 3);

    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("chexing.mp3", true);
    return true;
}

void WaitScene::menuEnterCallback(Ref* pSender)
{
   auto scene = GroupScene::createScene();
    Director::getInstance()->pushScene(TransitionCrossFade::create(.3, scene));
}

void WaitScene::onConnected(void) {
    Label* infoLabel = (Label*) this->getChildByTag(TAG_INFO_LABEL);
    infoLabel->setVisible(false);
    infoLabel->setString("");
}

void WaitScene::onDisconnected(void) {
    Label* infoLabel = (Label*) this->getChildByTag(TAG_INFO_LABEL);
    infoLabel->setVisible(true);
    infoLabel->setString("网络连接已断开");
}

void WaitScene::onError(int errCode) {
    Label* infoLabel = (Label*) this->getChildByTag(TAG_INFO_LABEL);
    infoLabel->setVisible(true);
    char buff[32] = {0};
    sprintf(buff, "网络连接错误码 = %d", errCode);
    infoLabel->setString(buff);
}

void WaitScene::onEnter () {
    Scene::onEnter();
    registerKeyPadListener();
    NetworkControler::getInstance()->setStatusDelegator(this);
}

void WaitScene::onExit() {
    Director::getInstance()->getEventDispatcher()->removeEventListener(mKeyboardListener);
    NetworkControler::getInstance()->setStatusDelegator(NULL);
    Scene::onExit();
}

void WaitScene::registerKeyPadListener () {
    mKeyboardListener = EventListenerKeyboard::create();
    mKeyboardListener->onKeyReleased =CC_CALLBACK_2(WaitScene::onKeyboardReleased, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(mKeyboardListener, 2);
}

void WaitScene::onKeyboardReleased(EventKeyboard::KeyCode keyCode, Event* e) {
    CCLOG("keycode pressed = %d", keyCode);
    if (keyCode == (EventKeyboard::KeyCode) 59 ||
        keyCode == (EventKeyboard::KeyCode) 125 ||
        keyCode == (EventKeyboard::KeyCode) 142 ||
        keyCode == (EventKeyboard::KeyCode) 164) {
        this->menuEnterCallback(NULL);
    }
}


