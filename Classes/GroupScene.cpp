#include "GroupScene.h"
#include "SimpleAudioEngine.h"
#include "VoteScene.h"
#include "Utility.h"

#define TAG_STAR_THUMB_0            (111)
#define TAG_STAR_LABEL_0            (142)
#define TAG_STAR_2                  (173)
#define TAG_ENVOLOPE                (115)
#define TAG_REWARD_PIC              (200)
#define TAG_REWARD_PIC_FRAME        (220)

#define TAG_INFO_LABEL      (128)
#define LABEL_OFFSET_Y      (-205)

USING_NS_CC;

static string JY_STARS[] = {"明星1组", "明星2组", "明星3组"};

Scene* GroupScene::createScene()
{
    return GroupScene::create();
}

// on "init" you need to initialize your instance
bool GroupScene::init()
{
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto spriteBg = Sprite::create("bg_1.png");
    spriteBg->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
    this->addChild(spriteBg, 0);
    
    Sprite* spTitle = Sprite::create("group_titi.png");
    spTitle->setPosition(Vec2(visibleSize.width * 0.5, visibleSize.height * 0.9));
    this->addChild(spTitle, 0);
    
    Sprite* evolope = Sprite::create("envolop.png");
    evolope->setPosition(Vec2(visibleSize.width * 0.5, visibleSize.height * 0.70));
    evolope->setTag(TAG_ENVOLOPE);
    this->addChild(evolope, 0);
    
    float space_0 = 20;
    Sprite* stampLeft = Sprite::create("stamp.png");
    stampLeft->setPosition(Vec2(space_0, visibleSize.height - space_0));
    stampLeft->setAnchorPoint(Vec2(0, 1));
    this->addChild(stampLeft, 0);
    
    Sprite* stampRight = Sprite::create("stamp.png");
    stampRight->setPosition(Vec2(visibleSize.width - space_0, visibleSize.height - space_0));
    stampRight->setAnchorPoint(Vec2(1, 1));
    this->addChild(stampRight, 0);
    
    float xSpace = 42;
    float ySpace = 50;
    float barCodeScale = 0.7;

#if FORMAL_SERVER
    std::string barcodeFile = "vote_barcode.png";
#else
    std::string barcodeFile = "vote_barcode_d.png";
#endif

    Sprite* voteBarcodeLeft = Sprite::create(barcodeFile);
    voteBarcodeLeft->setPosition(Vec2(xSpace, visibleSize.height - ySpace));
    voteBarcodeLeft->setAnchorPoint(Vec2(0,1));
    voteBarcodeLeft->setScale(barCodeScale);
    this->addChild(voteBarcodeLeft, 1);
    
    Sprite* voteBarcodeRight = Sprite::create(barcodeFile);
    voteBarcodeRight->setPosition(Vec2(visibleSize.width - xSpace, visibleSize.height - ySpace));
    voteBarcodeRight->setAnchorPoint(Vec2(1,1));
    voteBarcodeRight->setScale(barCodeScale);
    this->addChild(voteBarcodeRight, 1);
    
    Sprite* logoLeft = Sprite::create("logo_left.png");
    logoLeft->setPosition(Vec2(10, 10));
    logoLeft->setAnchorPoint(Vec2(0, 0));
    this->addChild(logoLeft, 1);
    
    Sprite* logoRight = Sprite::create("logo_right.png");
    logoRight->setPosition(Vec2(visibleSize.width - 10, 10));
    logoRight->setAnchorPoint(Vec2(1, 0));
    this->addChild(logoRight, 1);
    
    auto starItem0 = MenuItemImage::create(
                                           "star_thumb_0.png",
                                           "star_thumb_0.png",
                                           CC_CALLBACK_1(GroupScene::menuStarCallback, this));
   
    float x = origin.x + visibleSize.width * 0.3;
    float y = origin.y + visibleSize.height * 0.5;

    starItem0->setPosition(Vec2(x, y));
    starItem0->setTag(TAG_STAR_THUMB_0);
    
    MenuItemImage* mil0 = MenuItemImage::create("star_label_0.png",
                                                "star_label_0.png",
                                                CC_CALLBACK_1(GroupScene::menuStarCallback, this));
    
    mil0->setPosition(Vec2(starItem0->getPosition().x , starItem0->getPosition().y + LABEL_OFFSET_Y));
    mil0->setTag(TAG_STAR_LABEL_0);

    auto starItem1 = MenuItemImage::create(
        "star_thumb_1.png",
        "star_thumb_1.png",
        CC_CALLBACK_1(GroupScene::menuStarCallback, this));

    x = origin.x + visibleSize.width * 0.7;
    starItem1->setPosition(Vec2(x, y));
    starItem1->setTag(TAG_STAR_THUMB_0 + 1);

    MenuItemImage* mil1 = MenuItemImage::create("star_label_1.png",
                                                "star_label_1.png",
                                                CC_CALLBACK_1(GroupScene::menuStarCallback, this));
    mil1->setPosition(Vec2(starItem1->getPosition().x, starItem1->getPosition().y + LABEL_OFFSET_Y));
    mil1->setTag(TAG_STAR_LABEL_0 + 1);

    auto starItem2 = MenuItemImage::create(
        "star_thumb_2.png",
        "star_thumb_2.png",
        CC_CALLBACK_1(GroupScene::menuStarCallback, this));

    starItem2->setPosition(Vec2(visibleSize.width * 0.5, visibleSize.height * 0.3));
    starItem2->setTag(TAG_STAR_THUMB_0 + 2);

    MenuItemImage* mil2 = MenuItemImage::create("star_label_2.png",
                                                "star_label_2.png",
                                                CC_CALLBACK_1(GroupScene::menuStarCallback, this));
    mil2->setPosition(Vec2(starItem2->getPosition().x, starItem2->getPosition().y + LABEL_OFFSET_Y));
    mil2->setTag(TAG_STAR_LABEL_0 + 2);

    // create menu, it's an autorelease object
    mMenu = Menu::create(starItem0, mil0, starItem1, mil1, starItem2, mil2, NULL);
    mMenu->setPosition(Vec2::ZERO);
    this->addChild(mMenu, 1);

    Label* infoLabel = Label::createWithTTF("", "fonts/ms_yahei.ttf", 24);
    infoLabel->setPosition(visibleSize.width * 0.5, visibleSize.height * 0.5);
    infoLabel->setVisible(false);
    infoLabel->setTag(TAG_INFO_LABEL);
    this->addChild(infoLabel, 3);
    
    this->createVersionDisplay();
    return true;
}

void GroupScene::menuStarCallback(Ref* pSender) {
    MenuItem* item = (MenuItem*)pSender;
    int starIndex = item->getTag() - TAG_STAR_THUMB_0;
    if (starIndex > 10) {
        starIndex = item->getTag() - TAG_STAR_LABEL_0;
    }
    this->toVoteScene(starIndex);
}

void GroupScene::toVoteScene (int starIndex) {
    CCLOG("toVoteScene %d", starIndex);

    MenuItem* itemThumb = (MenuItem*) mMenu->getChildByTag(TAG_STAR_THUMB_0 + starIndex);
    if (!itemThumb->isEnabled()) {
        CCLOG("group %d already voted", starIndex);
        return;
    }
    
    MenuItem* itemLabel = (MenuItem*) mMenu->getChildByTag(TAG_STAR_LABEL_0 + starIndex);
    itemThumb->setEnabled(false);
    itemLabel->setEnabled(false);
    
    std::string title;
    
    title = JY_STARS [starIndex];
    
    VoteScene* scene = (VoteScene*) VoteScene::createScene();
    Director::getInstance()->pushScene(TransitionCrossFade::create(.3, scene));
    scene->initWithStarId(starIndex, title);
}

void GroupScene::onConnected(void) {
    Label* infoLabel = (Label*) this->getChildByTag(TAG_INFO_LABEL);
    infoLabel->setVisible(false);
    infoLabel->setString("");
}

void GroupScene::onDisconnected(void) {
    Label* infoLabel = (Label*) this->getChildByTag(TAG_INFO_LABEL);
    infoLabel->setVisible(true);
    infoLabel->setString("网络连接已断开");
}

void GroupScene::onError(int errCode) {
    Label* infoLabel = (Label*) this->getChildByTag(TAG_INFO_LABEL);
    infoLabel->setVisible(true);
    char buff[32] = { 0 };
    sprintf(buff, "网络连接错误码 = %d", errCode);
    infoLabel->setString(buff);
}

void GroupScene::onEnter() {
    Scene::onEnter();
    this->displayResult();
    this->registerKeyPadListener();
    NetworkControler::getInstance()->setStatusDelegator(this);
}

void GroupScene::createResultPic(int index, Sprite* pic) {
    if (this->getChildByTag(TAG_REWARD_PIC + index)) {
        return;
    }
    Sprite* frame = Sprite::create("pic_bg.png");
    frame->setPosition(mPicPosotions[index]);
    frame->setTag(TAG_REWARD_PIC + index);
    this->addChild(frame, 2);
    Size size = frame->getBoundingBox().size;
    pic->setPosition(Vec2(size.width * 0.5, size.height * 0.563));
    auto visibleSize = Director::getInstance()->getVisibleSize();

    pic->setScale(Utility::getSuitableScale(pic, Size(visibleSize.width*0.113, visibleSize.height*0.183)));
    frame->addChild(pic, 3);
}

void GroupScene::displayResult () {
    std::vector<std::string> result0 = Utility::getInstance()->getVoteResult(0);
    std::vector<std::string> result1 = Utility::getInstance()->getVoteResult(1);
    std::vector<std::string> result2 = Utility::getInstance()->getVoteResult(2);
    
    if (result0.size() == 0 && result1.size() == 0 && result2.size() == 0) {
        return;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    float y = visibleSize.height * 0.6;

    float picOffsetY = - visibleSize.height * 0.35;
    float secSpace = visibleSize.width * 0.25;
    
    
    float pic_w = visibleSize.width * 0.78 / 5;
    for (int i=0; i<6; i++) {
        mPicPosotions[i] = Vec2 (visibleSize.width * 0.11 + pic_w * i, visibleSize.height * 0.23);
    }
    
    Sprite* evolopeRight = Sprite::create("envolop_right.png");
    evolopeRight->setPosition(Vec2(visibleSize.width * 0.93, visibleSize.height * 0.5));
    this->addChild(evolopeRight, 0);
    
    Sprite* evolopeLeft = Sprite::create("envolop_left.png");
    evolopeLeft->setPosition(Vec2(visibleSize.width * 0.07, visibleSize.height * 0.5));
    this->addChild(evolopeLeft, 0);
    
    Sprite* evolope = (Sprite*) this->getChildByTag(TAG_ENVOLOPE);
    evolope->setVisible(false);
    
    MenuItem* itemThumb0 = (MenuItem*) mMenu->getChildByTag(TAG_STAR_THUMB_0);
    itemThumb0->setPosition(Vec2(visibleSize.width * 0.5 - secSpace, y));
    MenuItem* itemLabel0 = (MenuItem*) mMenu->getChildByTag(TAG_STAR_LABEL_0);
    
    if (result0.size()) {
        itemLabel0->setVisible(false);
        
        Sprite* pic0 = Sprite::create(result0[0]);
        if (pic0) {
            this->createResultPic(0, pic0);
        }
        
        if (result0.size() > 1) {
            Sprite* pic1 = Sprite::create(result0[1]);
            if (pic1) {
                this->createResultPic(1, pic1);
            }
        }
    } else {
        itemLabel0->setPosition(Vec2(itemThumb0->getPosition().x , itemThumb0->getPosition().y + LABEL_OFFSET_Y));
    }
    
    MenuItem* itemThumb1 = (MenuItem*) mMenu->getChildByTag(TAG_STAR_THUMB_0 + 1);
    itemThumb1->setPosition(Vec2(visibleSize.width * 0.5, y));
    MenuItem* itemLabel1 = (MenuItem*) mMenu->getChildByTag(TAG_STAR_LABEL_0 + 1);
    
    if (result1.size()) {
        itemLabel1->setVisible(false);
        
        Sprite* pic0 = Sprite::create(result1[0]);
        if (pic0) {
            this->createResultPic(2, pic0);
        }
        
        if (result1.size() > 1) {
            Sprite* pic1 = Sprite::create(result1[1]);
            if (pic1) {
                this->createResultPic(3, pic1);
            }
        }
    } else {
        itemLabel1->setPosition(Vec2(itemThumb1->getPosition().x , itemThumb1->getPosition().y + LABEL_OFFSET_Y));
    }
    
    MenuItem* itemThumb2 = (MenuItem*) mMenu->getChildByTag(TAG_STAR_THUMB_0 + 2);
    itemThumb2->setPosition(Vec2(visibleSize.width * 0.5 + secSpace, y));
    MenuItem* itemLabel2 = (MenuItem*) mMenu->getChildByTag(TAG_STAR_LABEL_0 + 2);
    
    if (result2.size()) {
        itemLabel2->setVisible(false);

Sprite* pic0 = Sprite::create(result2[0]);
if (pic0) {
    this->createResultPic(4, pic0);
}

if (result2.size() > 1) {
    Sprite* pic1 = Sprite::create(result2[1]);
    if (pic1) {
        this->createResultPic(5, pic1);
    }
}
    }
 else {
     itemLabel2->setPosition(Vec2(itemThumb2->getPosition().x, itemThumb2->getPosition().y + LABEL_OFFSET_Y));
 }

 if (itemLabel0->isVisible() == false && itemLabel1->isVisible() == false && itemLabel2->isVisible() == false) {// all finished!
     Sprite* cong = Sprite::create("cong.png");
     cong->setPosition(Vec2(visibleSize.width * 0.5, visibleSize.height * 0.05));
     this->addChild(cong, 0);
     cong->runAction(Blink::create(100, 50));
     NetworkControler::getInstance()->finishVote();
 }
}

void  GroupScene::onExit() {
    CCLOG("GroupScene::onExit");
    Director::getInstance()->getEventDispatcher()->removeEventListener(mKeyboardListener);
    NetworkControler::getInstance()->setStatusDelegator(NULL);
    Scene::onExit();
}

void GroupScene::registerKeyPadListener() {
    mKeyboardListener = EventListenerKeyboard::create();
    mKeyboardListener->onKeyReleased = CC_CALLBACK_2(GroupScene::onKeyboardReleased, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(mKeyboardListener, 2);
}

void  GroupScene::ScaleOutIn(int index) {
    Sprite* pics[6] = { 0 };
    int outIndex = -1;
    for (int i = 0; i < 6; i++) {
        pics[i] = (Sprite*) this->getChildByTag(TAG_REWARD_PIC + i);
        if (pics[i]) {
            if (pics[i]->getNumberOfRunningActions() > 0) {
                return; // cancel if action is running
            }
            if(pics[i]->getUserData()) {
                outIndex = i;
            }
        }
    }

    if (outIndex != -1) { // 有放大的先把放大的收起来, 避免同时放大多个
        index = outIndex;
    }

    if (pics[index] == NULL) {
        return;
    }

    float gSc = 3.0;
    Vec2* originPos = (Vec2*)pics[index]->getUserData();

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 sTargetPosition = Vec2(visibleSize.width * 0.5, visibleSize.height * 0.48);

    //pics[index]->stopAllActions();

    if (pics[index]->getUserData() == NULL) {
        pics[index]->setZOrder(4);
        pics[index]->setUserData(new Vec2(mPicPosotions[index]));
        CallFunc* callback = CallFunc::create([&]() {
        });

        Spawn* spawn = Spawn::create(ScaleTo::create(.3, gSc), MoveTo::create(0.3, sTargetPosition), callback, NULL);
        pics[index]->runAction(spawn);
    }
    else {
        CallFunc* callback = CallFunc::create([&]() {
            for (int i = 0; i < 6; i++) {
                pics[i] = (Sprite*) this->getChildByTag(TAG_REWARD_PIC + i);
                if (pics[i]) {
                    pics[i]->setZOrder(2);
                }
            }
        });
        Spawn* reverseSpawn = Spawn::create(ScaleTo::create(.3, 1.0), MoveTo::create(.3, mPicPosotions[index]), callback, NULL);
        pics[index]->runAction(reverseSpawn);
        pics[index]->setUserData(NULL);
    }

    for (int i = 0; i < 6; i++) { // reset other position
        if (i == index || pics[i] == NULL) {
            continue;
        }
        pics[i]->setPosition(mPicPosotions[i]);
    }
}

void GroupScene::onKeyboardReleased(EventKeyboard::KeyCode keyCode, Event* e) {
    CCLOG("keycode pressed = %d", keyCode);
    if (mKeyBuff.size() >= 2) {
        mKeyBuff.clear();
    }

    mKeyBuff.push_back(keyCode);

    if (mKeyBuff.size() == 1) {
        if (mKeyBuff[0] >= (EventKeyboard::KeyCode) 77 && mKeyBuff[0] <= (EventKeyboard::KeyCode) 82) {
            int candIndex = (int)mKeyBuff[0] - 77;
            this->ScaleOutIn(candIndex);
            mKeyBuff.clear();
        }
        else if (mKeyBuff[0] != (EventKeyboard::KeyCode) 141
            && mKeyBuff[0] != (EventKeyboard::KeyCode) 133
            && mKeyBuff[0] != (EventKeyboard::KeyCode) 149) {
            mKeyBuff.clear();
        }
    } else if (mKeyBuff.size() == 2) {
        if (mKeyBuff [0] == (EventKeyboard::KeyCode) 141 && // R
            mKeyBuff [1] == (EventKeyboard::KeyCode) 125) { // B
            toVoteScene(0);
        } else if (mKeyBuff [0] == (EventKeyboard::KeyCode) 133 && // J
                   mKeyBuff [1] == (EventKeyboard::KeyCode) 134) { // K
            toVoteScene(1);
        } else if (mKeyBuff [0] == (EventKeyboard::KeyCode) 149 && // Z
                   mKeyBuff [1] == (EventKeyboard::KeyCode) 133) { // J
            toVoteScene(2);
        } else {
            mKeyBuff.clear();
        }
    }
}

void GroupScene::createVersionDisplay() {
    Label* versionLable = Label::createWithSystemFont(Application::getInstance()->getVersion(), "Arial", 32);
    versionLable->setColor(Color3B::WHITE);
    versionLable->setPosition(Vec2(Director::getInstance()->getVisibleSize().width * 0.995, 0));
    versionLable->setAnchorPoint(Vec2(1, 0));
    this->addChild(versionLable);
}