#ifndef __WaitScene_SCENE_H__
#define __WaitScene_SCENE_H__

#include "cocos2d.h"
#include "NetworkControler.h"
#include "CCEventKeyboard.h"

class WaitScene : public cocos2d::Scene, NetworkStatusDelegate
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    virtual void onEnter ();
    virtual void onExit ();
    // a selector callback
    void menuEnterCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(WaitScene);

    virtual void onConnected(void);
    virtual void onDisconnected(void);
    virtual void onError(int errCode);
private:
    void registerKeyPadListener();
    void onKeyboardReleased(EventKeyboard::KeyCode keyCode, Event* e);
    EventListenerKeyboard* mKeyboardListener;
};

#endif // __WaitScene_SCENE_H__
