#include "Utility.h"
#include <cstdlib>
#include <stdlib.h>
#include "VoteScene.h"


#include "json/rapidjson.h"

#include "json/document.h"

#include "json/writer.h"

#include "json/stringbuffer.h"


Utility* Utility::sharedUtility = nullptr;

Utility* Utility::getInstance(){
    if(sharedUtility == NULL) {
        sharedUtility = new Utility();
    }
    return sharedUtility;
}

Utility::Utility () {
    // default value
    mVoteTimeInSec = 19; 
}

int Utility::getVoteTimeInSec() {
    return mVoteTimeInSec;
}

std::string Utility::getLocalUsablePath() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    std::string path = FileUtils::getInstance()->getSearchPaths()[0];
    int n = path.find_last_of("/");
    path = path.erase(n, path.length() - n);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    std::string path = FileUtils::getInstance()->getWritablePath();
    int n = path.find_last_of("/");
    if (n == path.length() - 1) {
        path = path.erase(path.length() - 1, 1);
    }
#endif
    return path;
}

void Utility::readSettingFile () {
    std::string path = this->getLocalUsablePath();

    char buff[256] = { 0 };
    sprintf(buff, "%s/jy.ini", path.c_str());

    std::string tmp = FileUtils::getInstance()->getStringFromFile(buff);
    CCLOG("%s", tmp.c_str());

    rapidjson::Document d;
    d.Parse<rapidjson::kParseDefaultFlags>(tmp.c_str());

    if (d.HasParseError()) {
        CCLOG("GetParseError %d\n", d.GetParseError());
        return;
    }

    if (d.IsObject()) {
        if (d.HasMember("vote_time_in_sec")) {
            mVoteTimeInSec = d["vote_time_in_sec"].GetInt();
        }

        // 数组
     /*   if (d.HasMember("array")) {
            for (int i = 0; i < d["array"].Size(); i++) {
                CCLOG("%d : %d", i, d["array"][i].GetInt());
            }
        }*/
    }
}

void Utility::setVoteResult(int groupId, string path0, string path1) {
    switch (groupId) {
    case 0:
        mGroupResult0.clear();
        mGroupResult0.push_back(path0);
        mGroupResult0.push_back(path1);
        break;
    case 1:
        mGroupResult1.clear();
        mGroupResult1.push_back(path0);
        mGroupResult1.push_back(path1);
        break;
    case 2:
        mGroupResult2.clear();
        mGroupResult2.push_back(path0);
        mGroupResult2.push_back(path1);
        break;
    }
}

vector<std::string> Utility::getVoteResult (int groupId) {
    switch (groupId) {
    case 0:
        return mGroupResult0;
        break;
    case 1:
        return mGroupResult1;
        break;
    case 2:
        return mGroupResult2;
        break;
    }
    return mGroupResult0;
}

float Utility::getSuitableScale(Sprite* sprite, Size container) {
    float scale = 30;
    Size size = sprite->getContentSize();
    if (size.height > size.width) {
        scale = container.height / size.height;
    } else {
        scale = container.width / size.width;
    }
    return scale;
}
